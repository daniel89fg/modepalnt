<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\ModePalnt;

/**
 * Composer autoload.
 */
require_once __DIR__ . '/vendor/autoload.php';

use FacturaScripts\Core\Base\InitClass;

/**
 * Description of Init
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        $this->loadExtension(new Extension\Controller\ListProyecto());
        $this->loadExtension(new Extension\Controller\ListProducto());
    }

    public function update()
    {
        
    }
}