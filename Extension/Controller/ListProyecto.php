<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\ModePalnt\Extension\Controller;
use PhpOffice\PhpSpreadsheet\IOFactory;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Plugins\Proyectos\Model\Proyecto;
use FacturaScripts\Plugins\Proyectos\Model\TareaProyecto;
use FacturaScripts\Core\Model\Cliente;
use FacturaScripts\Core\Model\Variante;
use FacturaScripts\Core\Model\Producto;
use FacturaScripts\Core\Model\Proveedor;
use FacturaScripts\Core\Model\PedidoProveedor;
use FacturaScripts\Core\Lib\BusinessDocumentTools;
use FacturaScripts\Plugins\OrdenesProduccion\Model\Orden;
use DateTime;

/**
 * Description of ListProyecto
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class ListProyecto
{    
   /**
     *
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction()
    {
        return function($action) {
            switch ($action) {
                case 'upload':
                    return $this->uploadProject();

                default:
                    return parent::execPreviousAction($action);
            }
        };
    }
    
    protected function uploadProject() {
        return function() {
            $uploadFile = $this->request->files->get('path');
            switch ($uploadFile->getMimeType()) {
                case 'application/vnd.ms-excel':
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    # Recomiendo poner la ruta absoluta si no está junto al script
                    # Nota: no necesariamente tiene que tener la extensión XLSX
                    $documento = IOFactory::load($uploadFile->getPathName());
                    
                    # Recuerda que un documento puede tener múltiples hojas
                    # obtener conteo e iterar
                    $totalDeHojas = $documento->getSheetCount();
                    
                    $procesos = [6, 1, 2, 7, 8, 3, 4, 5];
                    
                    $this->dataBase->beginTransaction();
                    try {
                        # Iterar hoja por hoja
                        for ($indiceHoja = 0; $indiceHoja < $totalDeHojas; $indiceHoja++) {
                            $materiales = [];

                            # Obtener hoja en el índice que vaya del ciclo
                            $hojaActual = $documento->getSheet($indiceHoja);

                            //CREAR PROYECTO
                            $proyecto = new Proyecto();
                            $proyecto->nombre = $hojaActual->getCell('E10')->getValue();
                            $fechainicio = DateTime::createFromFormat('d/m/Y', $hojaActual->getCell('G13')->getFormattedValue());
                            $proyecto->fechainicio = $fechainicio->format('Y-m-d');
                            $fechafin = DateTime::createFromFormat('d/m/Y', $hojaActual->getCell('K13')->getFormattedValue());
                            $proyecto->fechafin = $fechafin->format('Y-m-d');
                            $proyecto->idempresa = $this->request->cookies->get('fsCompany');
                            $proyecto->nick = $this->request->cookies->get('fsNick');

                            $client = new Cliente();
                            $whereC = [new DataBaseWhere('nombre', $hojaActual->getCell('A6')->getValue())];
                            $client->loadFromCode('', $whereC);
                            
                            if (is_null($client->codcliente)) {
                                $client->nombre = $hojaActual->getCell('A6')->getValue();
                                $client->cifnif = '';
                                $client->save();
                            }
                            
                            $prov = new Proveedor();
                            $whereP = [new DataBaseWhere('nombre', $hojaActual->getCell('A6')->getValue())];
                            $prov->loadFromCode('', $whereP);
                            
                            if (is_null($prov->codproveedor)) {
                                $prov->nombre = $hojaActual->getCell('A6')->getValue();
                                $prov->cifnif = '';
                                $prov->save();
                            }
                            
                            $proyecto->codcliente = $client->codcliente;

                            if ($proyecto->save()){
                                # obtener filas y columnas máximas
                                $highestColumm = $hojaActual->getHighestColumn();
                                $highestRow = $hojaActual->getHighestRow();

                                for ($fila = 16; $fila <= $highestRow; $fila++) {
                                    //REFERENCIA PRODUCTO -> TAREA
                                    $referenciaProducto = trim($hojaActual->getCell('B'.$fila)->getValue());
                                    
                                    if ($referenciaProducto != '' && strtolower($referenciaProducto) != 'nº producto') {
                                        //CREA TAREA
                                        $tarea = new TareaProyecto();
                                        $tarea->cantidad = (int)$hojaActual->getCell('L'.$fila)->getValue();
                                        $tarea->fechainicio = $proyecto->fechainicio;
                                        $tarea->fechafin = $proyecto->fechafin;
                                        $tarea->idproyecto = $proyecto->idproyecto;

                                        $variante = new Variante();
                                        $whereV = [new DataBaseWhere('referencia', $referenciaProducto)];
                                        $variante->loadFromCode('', $whereV);
                                        
                                        if ($variante->idvariante) {
                                            $producto = new Producto();
                                            $producto->loadFromCode($variante->idproducto);
                                            //$tarea->idvariante = $variante->idvariante;
                                            $nombreT = $producto->descripcion;
                                        } else {
                                            $nombreT = $hojaActual->getCell('D'.$fila)->getValue();
                                        }
                                        
                                        $tarea->nombre = $nombreT;
                                        if ($tarea->save()) {
                                            foreach ($procesos as $idproceso) {
                                                $orden = new Orden();
                                                $orden->idtarea = $tarea->idtarea;
                                                $orden->cantidadpedida = $tarea->cantidad;
                                                $orden->cantidadpendiente = 0;
                                                $orden->fechafinalizacion = $tarea->fechafin;
                                                $orden->estado = 0;
                                                $orden->cronometro = 0;
                                                $orden->idproceso = $idproceso;
                                                $orden->save();
                                            }
                                        }
                                    }
                                    
                                    //REFERENCIA PRODUCTO -> MATERIAL
                                    $referenciaMaterial = $hojaActual->getCell('C'.$fila)->getValue();
                                    
                                    if ($referenciaMaterial && strtolower($referenciaMaterial) != 'nº producto') {
                                        $materiales[] = array(
                                            'idproducto' => $referenciaMaterial,
                                            'descripcion' => $hojaActual->getCell('E'.$fila)->getValue(),
                                            'cantidad' => $hojaActual->getCell('L'.$fila)->getValue()
                                        );
                                    }
                                }
                                
                                //CREAR PEDIDO DE PROVEEDOR
                                $pedidoProveedor = new PedidoProveedor();
                                $pedidoProveedor->setSubject($prov);
                                $pedidoProveedor->setDate(date('d-m-Y'), date('H:i:s'));
                                $pedidoProveedor->idproyecto = $proyecto->idproyecto;
                                
                                if ($pedidoProveedor->save()) {
                                    //AÑADIMOS LINEAS AL PEDIDO PROVEEDOR
                                    foreach ($materiales as $material) {
                                        $newLinea = $pedidoProveedor->getNewProductLine($material['idproducto']);
                                        $newLinea->cantidad = $material['cantidad'];
                                        $newLinea->idpedido = $pedidoProveedor->idpedido;
                                        $newLinea->save();
                                    }

                                    //RECALCULAMOS PEDIDO PROVEEDOR
                                    $docTools = new BusinessDocumentTools();
                                    $docTools->recalculate($pedidoProveedor);
                                    $pedidoProveedor->save();
                                }
                            }
                        }
                        
                        $this->dataBase->commit();
                        $this->toolBox()->i18nLog()->notice('record-updated-correctly');
                    } catch (\Exception $e) {
                        $this->dataBase->rollback();
                        $this->toolBox()->i18nLog()->warning('record-save-error');
                    }
                    
                    break;

                default:
                    $this->toolBox()->i18nLog()->error('file-not-supported');
            }

            return;
        };
    }
}