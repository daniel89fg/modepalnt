<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\ModePalnt\Extension\Controller;
use PhpOffice\PhpSpreadsheet\IOFactory;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\Model\Producto;

/**
 * Description of ListProducto
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class ListProducto
{
   /**
     *
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction()
    {
        return function($action) {
            switch ($action) {
                case 'upload':
                    return $this->uploadProducts();

                default:
                    return parent::execPreviousAction($action);
            }
        };
    }
    
    protected function uploadProducts() {
        return function() {
            $uploadFile = $this->request->files->get('path');
            switch ($uploadFile->getMimeType()) {
                case 'application/vnd.ms-excel':
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    # Recomiendo poner la ruta absoluta si no está junto al script
                    # Nota: no necesariamente tiene que tener la extensión XLSX
                    $documento = IOFactory::load($uploadFile->getPathName());
                    
                    # Recuerda que un documento puede tener múltiples hojas
                    # obtener conteo e iterar
                    $totalDeHojas = $documento->getSheetCount();
                    
                    # Iterar hoja por hoja
                    $this->dataBase->beginTransaction();
                    try {
                        for ($indiceHoja = 0; $indiceHoja < $totalDeHojas; $indiceHoja++) {
                            # Obtener hoja en el índice que vaya del ciclo
                            $hojaActual = $documento->getSheet($indiceHoja);

                            # obtener filas y columnas máximas
                            $highestColumm = $hojaActual->getHighestColumn();
                            $highestRow = $hojaActual->getHighestRow();

                            for ($fila = 2; $fila <= $highestRow; $fila++) {
                                $referencia = $hojaActual->getCell('A'.$fila)->getValue();
                                
                                if ($referencia) {
                                    $producto = new Producto();
                                    $whereP = [new DataBaseWhere('referencia', $referencia)];
                                    $producto->loadFromCode('', $whereP);
                                    
                                    if (empty($producto->referencia)) {
                                        $producto->referencia = $hojaActual->getCell('A'.$fila)->getValue();
                                        $producto->descripcion = $hojaActual->getCell('B'.$fila)->getValue();
                                        $producto->save();
                                    }
                                }
                            }
                        }
                        
                        $this->dataBase->commit();
                        $this->toolBox()->i18nLog()->notice('record-updated-correctly');
                    } catch (\Exception $e) {
                        $this->dataBase->rollback();
                        $this->toolBox()->i18nLog()->warning('record-save-error');
                    }
                    
                    break;

                default:
                    $this->toolBox()->i18nLog()->error('file-not-supported');
            }

            return;
        };
    }
}